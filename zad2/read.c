/*
 *
 *  Upgraded on: 19.12.2019
 *      Author: MW
 */
#include "proj.h"// dodanie pliku naglowkowego projektu
void read(ListElement_type **head)//funkcja czytajaca z pliku
{
    FILE *fp;
    fp=fopen(plik,"r");//otwarcie pliku w trybie odczyty
    char *line = NULL;
    char *pole1;
    char *pole3;
    char *pole4;
    char *pole2;

    pole1=(char*)malloc(20*sizeof(char));
    pole3=(char*)malloc(20*sizeof(char));
    pole4=(char*)malloc(20*sizeof(char));
    pole2=(char*)malloc(3*sizeof(char));//alokacja zmiennych
    size_t len = 0;
    unsigned long read;//zmienne do obslugi getline'a

    if (fp == NULL) { //powiadomienie o nieudanej probie otwarcia pliku
        printf("Blad podczas funkcji fopen.\n");
        exit(EXIT_FAILURE);
    }




    while ((read = getline(&line, &len, fp)) != -1) {//czyta po linii do stringa line
        printf("%s", line);//wypisuje wczytana linijke z tekstu
        sscanf(line,"%[^,],%[^,],%[^,],%s", pole1,pole2,pole3,pole4);//formatowanie ze stringa na mniejsze stringi odpowiadajace odpowiednik polom
        add(head,pole1,pole2,pole3,pole4);//dodanie do listy nowego elementu
    }



    fclose(fp);//zamkniecie pliku

}
